from dash import Dash, dcc, html,Input, Output
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas
import plotly.graph_objects as go

app = Dash(__name__, external_stylesheets=[dbc.themes.CYBORG])
colors = {"background": "#111111", "text": "#7FDBFF"}
